package petrov.autotest;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class YandexSearchTest extends BaseTest{

    public String text = "погода пенза";

    private static SearchPage searchPage;

    @BeforeClass
    public void beforeTest(){
        searchPage = PageFactory.initElements(driver, SearchPage.class);
    }

    @Test
    public void searchTest(){
        driver.get("https://ya.ru/");
        searchPage.search(text);
        Assert.assertEquals(text, searchPage.getResult());
    }

}
