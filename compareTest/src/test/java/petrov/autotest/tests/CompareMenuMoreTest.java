package petrov.autotest.tests;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import petrov.autotest.pages.YandexCitySelectionPage;
import petrov.autotest.pages.YandexPage;

import java.util.List;

public class CompareMenuMoreTest extends BaseTest {


    private static YandexPage yandexPage;
    private static YandexCitySelectionPage yandexCitySelectionPage;

    @Test
    public void searchTest(){
        yandexPage = new YandexPage(driver);
        yandexCitySelectionPage = new YandexCitySelectionPage(driver);

        driver.get("https://yandex.by/");

        yandexPage.getCitySelectPage();
        yandexCitySelectionPage.selectCity("Анадырь");
        yandexPage.menuMoreClick();
        List<WebElement> moreCityOneList = yandexPage.getMenuMoreLinksList();

        yandexPage.getCitySelectPage();
        yandexCitySelectionPage.selectCity("Владивосток");
        yandexPage.menuMoreClick();
        List<WebElement> moreCityTwoList = yandexPage.getMenuMoreLinksList();

        Assert.assertEquals(moreCityOneList.size(), moreCityTwoList.size());

        for (int i = 0; i < moreCityOneList.size(); i++) {
            Assert.assertEquals(moreCityOneList.get(i).getAttribute("href"), moreCityTwoList.get(i).getAttribute("href"));
        }

    }

}
