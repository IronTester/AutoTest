package petrov.autotest.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class YandexCitySelectionPage {

    @FindBy(css = ".input__control")
    private WebElement cityInput;

    @FindBy(css = ".popup__content li:first-child")
    private WebElement firstLinkString;


    public WebDriver driver;

    public YandexCitySelectionPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }


    public void selectCity(String city){

        cityInput.clear();
        cityInput.sendKeys(city);
        firstLinkString.click();
    }

}
