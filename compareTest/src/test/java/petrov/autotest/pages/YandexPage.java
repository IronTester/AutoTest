package petrov.autotest.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;


public class YandexPage {

    @FindBy(css = ".link_geosuggest_yes")
    private WebElement citySelectLink;

    @FindBy(css = ".home-tabs__more-switcher")
    private WebElement menuMoreButton;

    @FindBy(css = ".home-tabs__more .home-link_black_yes")
    private List<WebElement> menuMoreLinks;


    public WebDriver driver;

    public YandexPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }


    public void getCitySelectPage(){
        citySelectLink.click();
    }

    public void menuMoreClick(){
        menuMoreButton.click();
    }

    public List<WebElement> getMenuMoreLinksList(){
        return menuMoreLinks;
    }

}
