package petrov.autotest.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ImagesYandexPage {

    @FindBy(css = ".image__image")
    private List<WebElement> imagesLinkList;


    public WebDriver driver;

    public ImagesYandexPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }


    public int getImagesLinkListSize(){
        return imagesLinkList.size();
    }

}
