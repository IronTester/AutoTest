package petrov.autotest.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class TranslateYandexPage {

    @FindBy(css = ".panel")
    private List<WebElement> translateLinkList;


    public WebDriver driver;

    public TranslateYandexPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }


    public int getTranslateLinkListSize(){
        return translateLinkList.size();
    }

}
