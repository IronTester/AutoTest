package petrov.autotest.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class NewsYandexPage {

    @FindBy(css = ".story__topic")
    private List<WebElement> newsLinkList;


    public WebDriver driver;

    public NewsYandexPage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }


    public int getNewsLinkListSize(){
        return newsLinkList.size();
    }

}
