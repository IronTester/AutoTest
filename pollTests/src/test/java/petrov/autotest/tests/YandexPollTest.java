package petrov.autotest.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import petrov.autotest.page.*;

public class YandexPollTest extends BaseTest {

    public static StartYandexMarketPage startYandexMarketPage;
    public static LaptopYandexMarketPage laptopYandexMarketPage;
    public static CompareYandexMarketPage compareYandexMarketPage;
    public static HomeMenuYandexPage homeMenuYandexPage;
    public static VideoYandexPage videoYandexPage;
    public static ImagesYandexPage imagesYandexPage;
    public static NewsYandexPage newsYandexPage;
    public static MapsYandexPage mapsYandexPage;
    public static MarketYandexPage marketYandexPage;
    public static TranslateYandexPage translateYandexPage;
    public static MusicYandexPage musicYandexPage;



    @Test
    public void ListProductsSizeTest(){

        int size;
        int sizeOne = 12;
        int sizeTwo = 48;

        startYandexMarketPage = new StartYandexMarketPage(driver);
        laptopYandexMarketPage = new LaptopYandexMarketPage(driver);

        driver.get("https://market.yandex.ru");

        startYandexMarketPage.selectLaptopTopMenuSubItem();
        laptopYandexMarketPage.selectListProductSizeOne();
        size = laptopYandexMarketPage.getProductsListCount();
        Assert.assertEquals(size, sizeOne);
        laptopYandexMarketPage.selectListProductSizeTwo();
        size = laptopYandexMarketPage.getProductsListCount();
        Assert.assertEquals(size, sizeTwo);

    }

    @Test
    public void ProductsSortingTest(){

        startYandexMarketPage = new StartYandexMarketPage(driver);
        laptopYandexMarketPage = new LaptopYandexMarketPage(driver);

        driver.get("https://market.yandex.ru");

        startYandexMarketPage.selectLaptopTopMenuSubItem();
        laptopYandexMarketPage.priceSortingButtonClick();
        Assert.assertTrue(laptopYandexMarketPage.isVisiblePriceSortingEnableButton());
        Assert.assertTrue(laptopYandexMarketPage.valideListProductsPriceSorting());

    }

    @Test
    public void compareMarketProductTest() throws InterruptedException {

        int count = 2;
        int countCompare;

        startYandexMarketPage = new StartYandexMarketPage(driver);
        laptopYandexMarketPage = new LaptopYandexMarketPage(driver);
        compareYandexMarketPage = new CompareYandexMarketPage(driver);

        driver.get("https://market.yandex.ru");

        startYandexMarketPage.selectLaptopTopMenuSubItem();
        laptopYandexMarketPage.addToCompareProductFirstInList();
        laptopYandexMarketPage.addToCompareProductSecondInList();
        laptopYandexMarketPage.compareButtonClick();
        countCompare = compareYandexMarketPage.getProductsCount();
        Assert.assertEquals(count, countCompare);
        compareYandexMarketPage.clearCompareList();
        countCompare = compareYandexMarketPage.getProductsCountClear();
        Assert.assertEquals(0, countCompare);

    }

    @Test
    public void homeMenuNavigateTest() throws InterruptedException {

        homeMenuYandexPage = new HomeMenuYandexPage(driver);
        videoYandexPage = new VideoYandexPage(driver);
        imagesYandexPage = new ImagesYandexPage(driver);
        newsYandexPage = new NewsYandexPage(driver);
        mapsYandexPage = new MapsYandexPage(driver);
        marketYandexPage = new MarketYandexPage(driver);
        translateYandexPage = new TranslateYandexPage(driver);
        musicYandexPage = new MusicYandexPage(driver);


        String urlHomeMenu;

        homeMenuYandexPage = new HomeMenuYandexPage(driver);

        driver.get("https://yandex.ru");

        urlHomeMenu = homeMenuYandexPage.getLinkVideoButton();
        homeMenuYandexPage.videoButtonClick();
        Assert.assertEquals(driver.getCurrentUrl(), urlHomeMenu);
        Assert.assertTrue(videoYandexPage.getVideoLinkListSize() > 0);
        driver.navigate().back();

        urlHomeMenu = homeMenuYandexPage.getLinkImagesButton();
        homeMenuYandexPage.imagesButtonClick();
        Assert.assertEquals(driver.getCurrentUrl(), urlHomeMenu);
        Assert.assertTrue(imagesYandexPage.getImagesLinkListSize() > 0);
        driver.navigate().back();

        urlHomeMenu = homeMenuYandexPage.getLinkNewsButton();
        homeMenuYandexPage.newsButtonClick();
        Assert.assertEquals(driver.getCurrentUrl(), urlHomeMenu);
        Assert.assertTrue(newsYandexPage.getNewsLinkListSize() > 0);
        driver.navigate().back();

        urlHomeMenu = homeMenuYandexPage.getLinkMapsButton();
        homeMenuYandexPage.mapsButtonClick();
        Assert.assertTrue(driver.getCurrentUrl().contains(urlHomeMenu));
        Assert.assertTrue(mapsYandexPage.getMapsLinkListSize() > 0);
        driver.navigate().back();

        urlHomeMenu = homeMenuYandexPage.getLinkMarketButton();
        homeMenuYandexPage.marketButtonClick();
        Assert.assertEquals(driver.getCurrentUrl(), urlHomeMenu);
        Assert.assertTrue(marketYandexPage.getMarketLinkListSize() > 0);
        driver.navigate().back();

        urlHomeMenu = homeMenuYandexPage.getLinkTranslateButton();
        homeMenuYandexPage.translateButtonClick();
        Assert.assertEquals(driver.getCurrentUrl(), urlHomeMenu);
        Assert.assertTrue(translateYandexPage.getTranslateLinkListSize() > 0);
        driver.navigate().back();

        urlHomeMenu = homeMenuYandexPage.getLinkMusicButton();
        homeMenuYandexPage.musicButtonClick();
        Assert.assertEquals(driver.getCurrentUrl(), urlHomeMenu);
        Assert.assertTrue(musicYandexPage.getMusicLinkListSize() > 0);
        driver.navigate().back();

    }

}
